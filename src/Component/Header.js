export default function Header() {
    return (
        <div>
            <div className="header">
                <img className="w-100 d-none header-img"  src="images/12.jpg" alt="sqr"/>
                <div className="container">
                    <div className="shop-rule">
                        <h1>There's a new way to shop in town.</h1>
                        <h5>Get personalized inspiration, exclusive deals and pay how you prefer. Only in the Klarna
                            app.</h5>
                        <p>Get the app directly to your phone:</p>
                        <div className="get-app d-flex">
                            <div className="img-sqr">
                                <img width={120} src="images/qr-code-74.png" alt="sqr"/>
                            </div>
                            <div className="line-column"></div>
                            <div className="form">
                                <form>
                                    <div className="omrs-input-group">
                                        <label className="omrs-input-underlined">
                                            <input required/>
                                            <span className="omrs-input-label">Phone numbers (+1)</span>
                                        </label>
                                    </div>
                                    <button className="btn-dark" type="submit">Send text</button>
                                </form>
                            </div>
                        </div>
                        <div className="get-the-app d-none">
                            <button className="btn-dark">Get the app</button>
                            <div className="downUp">
                                <img src="images/down-arrow.svg" alt="svg"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}