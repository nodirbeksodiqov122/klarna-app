import Fade from 'react-reveal/Fade';
import Carousel from "./Carousel";

export default function Section() {
    return (
        <div>
            <div className="section-service">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                <img src="images/1.png" alt="svg"/>
                                <h3>Pay how you like</h3>
                                <p> Enjoy the flexibility to get what you want and pay over time.</p>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img src="images/2.png" alt="svg"/>
                                <h3>4 interest-free payments</h3>
                                <p> Pay in 4 with Klarna through our app, with integrated brands, or anywhere Visa is
                                    accepted.</p>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img src="images/3.png" alt="svg"/>
                                <h3>No credit impact </h3>
                                <p>Our instant approval process has zero impact on your credit score.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="section-app">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <img className="w-100 promo-img" src="images/App_image_promo.jpg.webp" alt="img"/>
                            <img className="d-none w-100" src="images/HP_app_image_mobile.jpg" alt="svg"/>
                        </div>
                        <div className="col">

                            <div className="card">
                                <Fade right>
                                    <h1>The all-in-one shopping app.</h1>
                                </Fade>
                                <Fade right>
                                    <p>It doesn’t matter if you’re shopping in bed, on the go, or at your desk—we’ve got
                                        you
                                        covered from inspo and deals, to payments and delivery tracking.</p>
                                </Fade>
                                <Fade right>
                                    <div>
                                        <button className="btn-dark">Get the app</button>
                                        <button className="btn-light">Learn more</button>
                                    </div>
                                </Fade>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <Carousel/>
            <div className="section-product">
                <div className="container">
                    <h1>Clothes & fashion</h1>
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/Bose.jpg.webp" alt="svg"/>
                                <div className="d-flex">
                                    <p>Lorem ipsum dolor</p>
                                    <img src="images/right-arrow-angle.svg" alt="svg"/>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/Bose.jpg.webp" alt="svg"/>
                                <div className="d-flex">
                                    <p>Lorem ipsum dolor</p>
                                    <img src="images/right-arrow-angle.svg" alt="svg"/>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/Bose.jpg.webp" alt="svg"/>
                                <div className="d-flex">
                                    <p>Lorem ipsum dolor</p>
                                    <img src="images/right-arrow-angle.svg" alt="svg"/>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/Bose.jpg.webp" alt="svg"/>
                                <div className="d-flex">
                                    <p>Lorem ipsum dolor</p>
                                    <img src="images/right-arrow-angle.svg" alt="svg"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="show-more d-flex">
                        <p>Show more stories in Clothes & fashion</p>
                        <span className="icon icon-arrow-right"></span>
                    </div>

                </div>
            </div>
            <div className="section-community">
                <div className="container">
                    <h1>What our community thinks.</h1>
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/App_store-1.jpg.webp" alt="svg"/>
                                <div>
                                    <h2>Available on App Store.</h2>
                                    <p>Out of 211,000 reviews in the App Store, we've received an amazing average rating
                                        of
                                        4.8 stars.</p>
                                    <button className="btn-light">
                                        Get the app
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="card">
                                <img className="w-100" src="images/Google_play-1.jpg.webp" alt="svg"/>
                                <div>
                                    <h2>Available on Google Play.</h2>
                                    <p>Our users on Google Play have given us an average rating of 4.1 stars out of
                                        211,000
                                        reviews.</p>
                                    <button className="btn-light">
                                        Get the app
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="section-get-app">
                <div className="container">
                    <h1>Get more time to pay. Anywhere online.</h1>
                    <button className="btn-dark">Get the app</button>
                </div>
            </div>

        </div>
    )
}