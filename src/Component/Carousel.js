import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import {useState, useEffect} from 'react';
import Fade from 'react-reveal/Fade';

export default function Carousel() {

    const [responsive, setOptions] = useState(
        {
            0: {
                items: 1,
            },
            450: {
                items: 2,
            },
            600: {
                items: 3,
            },
            1200: {
                items: 4,
            },
        }
    )
    return (
        <div>
            <div className="owl container">
                <h1>Top details</h1>
                <OwlCarousel
                    className="owl-theme"
                    items="4"
                    dots={false}
                    margin={20}
                    autoplay={false}
                    nav={true}
                    responsive={responsive}
                >
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-1.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-2.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-3.jpg" alt="c-3"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-4.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-5.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-1.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/c-2.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                </OwlCarousel>
                <p className="commission">Klarna may get a commission.</p>
            </div>
            <div className="owl container">
                <h1>Featured stores</h1>
                <OwlCarousel
                    className="owl-theme"
                    items="4"
                    dots={false}
                    margin={20}
                    autoplay={false}
                    nav={true}
                    responsive={responsive}
                >
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-1.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-5.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-3.webp" alt="s-3"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-4.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-5.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-1.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                    <div className="item">
                        <Fade bottom>
                            <div className="box">
                                <img src="images/s-2.webp" alt="inst"/>
                                <div className="d-flex">
                                    <span>Lorem ipsum dolor</span>
                                    <img className="right-arrow" src="images/right-arrow-angle.svg" alt="inst"/>
                                </div>
                            </div>
                        </Fade>
                    </div>
                </OwlCarousel>
                <p className="commission">Klarna may get a commission.</p>
            </div>
        </div>
    )
}