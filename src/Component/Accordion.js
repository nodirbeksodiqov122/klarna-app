import React from 'react';

import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemButton,
    AccordionItemPanel,
} from 'react-accessible-accordion';

// Demo styles, see 'Styles' section below for some notes on use.
import 'react-accessible-accordion/dist/fancy-example.css';

export default function Example() {
    return (
        <div>
            <Accordion className="d-none">
                <AccordionItem>
                    <AccordionItemHeading>
                        <AccordionItemButton>
                            Customer
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                        <ul>
                            <li>Buy now pay later</li>
                            <li>Contact us via app</li>
                            <li>Customer service</li>
                            <li>Klarna stores</li>
                            <li>Shopping app</li>
                            <li>Rewards club</li>
                            <li>Feedback and complaints</li>
                            <li>Shopping inspiration</li>
                        </ul>
                    </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                    <AccordionItemHeading>
                        <AccordionItemButton>
                            Business
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                        <ul>
                            <li>Sell with Klarna</li>
                            <li>Payment methods</li>
                            <li> Platforms and partners</li>
                            <li> Partner program</li>
                            <li>Business login</li>
                            <li>Business support</li>
                            <li>Operational status</li>
                        </ul>
                    </AccordionItemPanel>
                </AccordionItem>
                <AccordionItem>
                    <AccordionItemHeading>
                        <AccordionItemButton>
                            Klarna
                        </AccordionItemButton>
                    </AccordionItemHeading>
                    <AccordionItemPanel>
                        <ul>
                            <li>About us</li>
                            <li>Careers</li>
                            <li> Legal</li>
                            <li> Press</li>
                            <li> Corporate social responsibility</li>
                            <li> Extra O blog</li>
                            <li> Privacy</li>
                            <li> Email connect</li>
                        </ul>
                    </AccordionItemPanel>
                </AccordionItem>
            </Accordion>
        </div>

    );
}