import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import Header from "./Component/Header";
import Section from "./Component/Section";
import Footer from "./Component/Footer";
import Navbar from "./Component/Navbar";

function App() {
  return (
    <div className="App">
      <Router>
                <Navbar/>
                <Switch>
                    <Route exact path="/">
                        <Header/>
                        <Section/>
                    </Route>
                </Switch>
                <Footer/>
            </Router>
    </div>
  );
}

export default App;
